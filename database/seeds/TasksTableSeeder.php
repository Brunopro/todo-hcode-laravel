<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //populando com dados dinamicamente com seed
       /* factory(App\Task::class)->create([
            'c_nametask' => 'Estudar bootstrap',
      
        ]);

        factory(App\Task::class)->create([
            'c_nametask' => 'Estudar inglês',
      
        ]);
        */
        factory(App\Task::class, 30)->create();
    }
}
