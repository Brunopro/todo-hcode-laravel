<?php

use Faker\Generator as Faker;

$factory->define(App\Task::class, function (Faker $faker) {
    return [
        'c_nametask' =>$faker->name,
        'n_stattask' =>$faker->boolean
    ];
});
