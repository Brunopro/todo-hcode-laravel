<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //permitir insersão de dados nas seguintes colunas
    protected $fillable = ['c_nametask', 'n_stattask'];

}
