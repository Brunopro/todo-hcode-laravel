<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

//adicionar esse importe para usar validacoes
use Illuminate\Contracts\Validation\Validator;

class TasksController extends Controller
{
    //retornar lista de tarefas
    public function index(){
        return Task::all();
    }

    public function store(Request $request){

       $request->validate([
           'c_nametask'=>'required|max:255',
           'n_stattask'=>'required'
       ]);

       $task = Task::create([
           'c_nametask' => $request->input('c_nametask'),
           'n_stattask' => $request->input('n_stattask')
        ]);

        return 'salvo com sucesso';
    }

    public function show(Task $task){
        return $task;
    }

    public function update(Request $request, Task $task)
    {

        $request->validate([
            'c_nametask'=>'required|max:255'
        ]);
        
        $task->c_nametask = $request->input('c_nametask');
        $task->save();

        return $task;

    }

    public function destroy(Task $task)
    {
        $task->delete();

        return response()->json(["success"=>true]);
    }
}
