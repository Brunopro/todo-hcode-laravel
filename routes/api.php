
<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//traz todos os registros
Route::get("/tasks",'TasksController@index');
//traz registro por id
Route::get("/tasks/{task}",'TasksController@show');

//inserindo registro
Route::post("/tasks",'TasksController@store');

//atualizando registro
Route::patch("/tasks/{task}",'TasksController@update');

//deletando registro
Route::delete('tasks/{task}', 'TasksController@destroy'); 
